"""Save attachments from a sender"""


import win32com.client
import click
import os
import logging

logging.basicConfig(level=logging.INFO)

@click.command()
@click.option('-m', '--mail', 'mail', help="Sender's mail")
@click.option('-p', '--path', 'path', help="Save path")
def main(mail: str, path: str) -> None:

    # Initialize list with all mail name used and a counter
    mail_list_name = []
    mail_counter = 0

    # Create a list with all files from the path
    files_list = os.listdir(path)

    # Get the first mail from the inbox
    outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
    inbox = outlook.GetDefaultFolder(6)
    messages = inbox.Items
    message = messages.GetFirst()

    while message:

        # Check if the mail is from the sender
        if message.SenderEmailAddress == mail:

            msg_name = message.Subject

            # Check if the mail have not been used
            if msg_name not in mail_list_name:

                logging.info(str(msg_name) + ": using...")

                # Add the mail name to the mail used list
                mail_list_name.append(msg_name)

                # Get the mail date
                mail_date = message.SentOn.date()

                # Get all attachments from the mail
                attachments = message.Attachments
                att_counter = attachments.Count

                if att_counter >= 1:

                    mail_counter += 1

                    if att_counter > 1:

                        for att in range(att_counter):

                            attachment = attachments.Item(1)
                            filename = str(mail_date) + "_" + str(mail_counter) + "_" + str(att + 1) + ".jpg"

                            # Check if the file have not been already saved
                            if filename not in files_list:
                                logging.info(filename + ": saving...")
                                attachment.SaveAsFile(path + '\\' + filename)
                            else:
                                logging.info(filename + ": already saved")

                    else:

                        attachment = attachments.Item(1)
                        filename = str(mail_date) + "_" + str(mail_counter) + ".jpg"

                        # Check if the file have not been already saved
                        if filename not in files_list:
                            logging.info(filename + ": saving...")
                            attachment.SaveAsFile(path + "\\" + filename)
                        else:
                            logging.info(filename + ": already saved")

            else:
                logging.info(str(msg_name) + ": already used")

        message = messages.GetNext()

    logging.info("All attachments have been saved")

if __name__ == "__main__":
    main()