# outlook_attachments

Save all attachments from a sender using the outlook app

# Requirements

1. pip install pypiwin32

2. pip install click

3. get the outlook app

# Run the python script

1. Have the outlook app open first

2. Write this command line into the command prompt: 

path\to\python.exe \path\to\the\script -m xxxxxx@xxx.xxx -p \path\to\save\attachments